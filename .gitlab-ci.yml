stages:
  - check
  - build
  - clean_up

# Triggered when the latest commit contains changes to cms/cvmfs/standalone
# Triggers the pipeline for every folder containing changes
check_changes_pipeline:
  stage: check
  rules:
    - changes: [cc7-cms/*, cc7-cvmfs/*, slc6-cms/*, slc6-cvmfs/*, slc5-cms/*, standalone/*]
      if: $CI_PIPELINE_SOURCE == 'push' # Trigger if push (for test on branches)
  tags:
      - docker # tags docker since those runners have git installed (needed by check.sh)
  script:
    - echo "Pipeline source - $CI_PIPELINE_SOURCE"
    - ./check.sh

# Test template for all images
# Tagged $DATE-$CI_COMMIT_SHORT_SHA
# Pushed to gitlab registry under name test
.test_all_template: &test_all
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - export DATE=$(date +"%Y-%m-%d")
    - if [[ $IMAGE_NAME == "standalone" ]]; then REPO_NAME=standalone; else REPO_NAME=$IMAGE_NAME; fi
    - echo "Pipeline source - $CI_PIPELINE_SOURCE"
    - echo "Building test image - $IMAGE_NAME"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}, \"$DOCKER_REGISTRY\":{\"auth\":\"${DOCKER_AUTH}\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/$REPO_NAME --dockerfile $CI_PROJECT_DIR/$REPO_NAME/Dockerfile --destination $CI_REGISTRY_IMAGE/test:$IMAGE_NAME-$DATE-$CI_COMMIT_SHORT_SHA --build-arg=BUILD_DATE="$DATE" --build-arg=VERSION="$DATE" --build-arg=VCS_URL="$CI_REPOSITORY_URL" --build-arg=VCS_REF="$CI_COMMIT_SHORT_SHA"

# Build template for cms/cvmfs images
# Tagged $DATE-$CI_COMMIT_SHORT_SHA and latest
# Pushed to gitlab registry and Docker Hub (only latest) 
.build_cms_cvmfs_template: &build_cms_cvmfs
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - export DATE=$(date +"%Y-%m-%d")
    - echo "Pipeline source - $CI_PIPELINE_SOURCE"
    - echo "Building image - $IMAGE_NAME"
    - export DOCKER_AUTH="$(echo -n $DOCKER_USER:$DOCKER_PASS | base64)" # https://github.com/GoogleContainerTools/kaniko#pushing-to-docker-hub
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}, \"$DOCKER_REGISTRY\":{\"auth\":\"${DOCKER_AUTH}\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/$IMAGE_NAME --dockerfile $CI_PROJECT_DIR/$IMAGE_NAME/Dockerfile --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$DATE-$CI_COMMIT_SHORT_SHA --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:latest --destination  $DOCKER_GROUP/$IMAGE_NAME:latest --build-arg=BUILD_DATE="$DATE" --build-arg=VERSION="$DATE" --build-arg=VCS_URL="$CI_REPOSITORY_URL" --build-arg=VCS_REF="$CI_COMMIT_SHORT_SHA"

# Build template for standalone images
# Tagged $DATE-$CI_COMMIT_SHORT_SHA
# Pushed to gitlab registry
.build_standalone_template: &build_standalone
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - export DATE=$(date +"%Y-%m-%d")
    - export IMAGE_NAME=$(echo $IMAGE_NAME | tr '[:upper:]' '[:lower:]') # IMAGE_NAME is given in lowercase to please docker image naming rules
    - echo "Pipeline source - $CI_PIPELINE_SOURCE"
    - echo "Building image - $IMAGE_NAME"
    - printf "Release - $RELEASE\nScram Arch - $SCRAM_ARCH\nBase image - $BASE_IMAGE\n"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR/$REPO_NAME --dockerfile $CI_PROJECT_DIR/$REPO_NAME/Dockerfile --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$DATE-$CI_COMMIT_SHORT_SHA --build-arg=BUILD_DATE="$DATE" --build-arg=VERSION="$DATE" --build-arg=VCS_URL="$CI_REPOSITORY_URL" --build-arg=VCS_REF="$CI_COMMIT_SHORT_SHA" --build-arg BASEIMAGE="$BASE_IMAGE" --build-arg SCRAM_ARCH="$SCRAM_ARCH" --build-arg CMSSW_VERSION="$RELEASE"
    - echo "export IMAGE_PATH=$CI_REGISTRY_IMAGE/$IMAGE_NAME:$DATE-$CI_COMMIT_SHORT_SHA" > standalone_image_path.env
  artifacts:
    paths:
      - standalone_image_path.env # Pass image path to clean_up job

test_all_pipeline:
  <<: *test_all
  stage: build
  only:
    variables:
      - $IMAGE_NAME
      - $TEST
  except:
    refs:
      - master
  tags:
    - docker-privileged

build_cms_cvmfs_pipeline:
  <<: *build_cms_cvmfs
  stage: build
  only:
    variables:
      - $IMAGE_NAME
    refs:
      - master
  variables:
    DOCKER_GROUP: cmscloud
    DOCKER_REGISTRY: https://index.docker.io/v1/
  tags:
    - docker-privileged

build_standalone_pipeline:
  <<: *build_standalone
  stage: build
  only:
    variables:
      - $RELEASE && $SCRAM_ARCH && $OS
    refs:
      - master
  tags:
    - docker-privileged-xl
  variables:
    IMAGE_NAME: $RELEASE-$SCRAM_ARCH
    REPO_NAME: standalone
    BASE_IMAGE: "$CI_REGISTRY_IMAGE/$OS-cms:latest"
  # Necessary variables to be passed to Dockerfile for building a standalone image is CMSSW_VERSION=$RELEASE,
  # SCRAM_ARCH=$SCRAM_ARCH and BASEIMAGE="gitlab-registry.cern.ch/cms-cloud/cmssw-docker/$OS-cms:latest"